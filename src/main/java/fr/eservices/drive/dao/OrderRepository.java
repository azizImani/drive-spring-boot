package fr.eservices.drive.dao;

import fr.eservices.drive.models.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {
}
