package fr.eservices.drive.dao;

import fr.eservices.drive.models.Article;
import org.springframework.data.repository.CrudRepository;

public interface ArticleRepository extends CrudRepository<Article, Integer> {

    Article save(Article article);

}
