package fr.eservices.drive.dao;

import fr.eservices.drive.models.StatusHistory;
import org.springframework.data.repository.CrudRepository;

public interface StatusHistoryRepository extends CrudRepository<StatusHistory, Integer> {
}
