package fr.eservices.drive.services;

import fr.eservices.drive.dao.ArticleRepository;
import fr.eservices.drive.dao.CategoryRepository;
import fr.eservices.drive.dao.StockRepository;
import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.Category;
import fr.eservices.drive.models.Perishable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class DefaultCatalogService implements CatalogService {

    @Autowired
    private CategoryRepository categoryRepo;
    @Autowired
    private ArticleRepository articleRepo;
    @Autowired
    private StockRepository stockRepository;

    @Override
    public List<Category> getCategories() {
        return categoryRepo.findAllByOrderByOrderIdxAsc();
    }

    @Override
    public List<Category> getArticleCategories(int id) {
        Optional<Article> article = articleRepo.findById(id);
        if(article.isPresent()) {
            return article.get().getCategories();
        }
        return Collections.emptyList();
    }

    @Override
    public List<Article> getCategoryContent(int categoryId) {
        Optional<Category> category = categoryRepo.findById(categoryId);
        if(category.isPresent()) {
            return category.get().getArticles();
        }
        return Collections.emptyList();
    }

    @Override
    public List<Perishable> getPerished(Date day) {
        // No need for this method for actual requirements
        return Collections.emptyList();
    }

    @Override
    public boolean isInStock(Article article) {
        return stockRepository.findByArticle(article).getQuantity() > 0;
    }
}
