package fr.eservices.drive.services;

import fr.eservices.drive.dao.CustomerRepository;
import fr.eservices.drive.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CustomerInitializer implements Initializer{

    @Autowired
    private CustomerRepository customerRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void init() {
        Customer customer = new Customer();
        customer.setEmail("john@gmail.com");
        customer.setFirstName("John");
        customer.setLastName("Doe");
        customer.setLogin("jdoe");
        customer.setPassword(passwordEncoder.encode("pass"));
        customerRepo.save(customer);
    }
}

