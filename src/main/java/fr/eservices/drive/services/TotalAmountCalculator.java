package fr.eservices.drive.services;

import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.Cart;
import fr.eservices.drive.models.CartEntry;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TotalAmountCalculator {

    public double totalPrices(Cart activeCart) {
        return totalPrices(activeCart.getCartEntries());
    }

    public double totalPrices(List<CartEntry> cartEntries) {
        double totalAmount = 0;
        for(CartEntry cartEntry : cartEntries){
            totalAmount += entryPrice(cartEntry);
        }
        return totalAmount;
    }

    private double articlePrice(Article article){
        return article.getPrice() +  article.getPrice() * article.getVat();
    }

    private double entryPrice(CartEntry cartEntry){
        return articlePrice(cartEntry.getArticle()) * cartEntry.getQuantity();
    }
}
