package fr.eservices.drive.services;

import fr.eservices.drive.dao.CategoryRepository;
import fr.eservices.drive.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CategoryDataInitializer implements Initializer{

    @Autowired
    private CategoryRepository categoryRepo;

    public void init(){

        {
            Category c = new Category();
            c.setId(1);
            c.setName("Boissons");
            c.setOrderIdx(2);
            categoryRepo.save(c);
        }

        {
            Category c = new Category();
            c.setId(2);
            c.setName("Viandes");
            c.setOrderIdx(3);
            categoryRepo.save(c);
        }

        {
            Category c = new Category();
            c.setId(3);
            c.setName("Saucisserie & farce");
            c.setOrderIdx(4);
            categoryRepo.save(c);
        }

        {
            Category c = new Category();
            c.setId(4);
            c.setName("Fruits & Legumes");
            c.setOrderIdx(1);
            categoryRepo.save(c);
        }

        {
            Category c = new Category();
            c.setId(5);
            c.setName("Production laitiere");
            c.setOrderIdx(5);
            categoryRepo.save(c);
        }

        {
            Category c = new Category();
            c.setId(6);
            c.setName("Divers");
            c.setOrderIdx(6);
            categoryRepo.save(c);
        }

    }
}
