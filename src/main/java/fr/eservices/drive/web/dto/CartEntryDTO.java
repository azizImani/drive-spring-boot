package fr.eservices.drive.web.dto;

public class CartEntryDTO {
    private int articleId;
    private int quantity;

    public CartEntryDTO(){}

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }
}
