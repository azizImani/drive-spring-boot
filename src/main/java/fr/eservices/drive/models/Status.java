package fr.eservices.drive.models;

public enum Status {
    ORDERED, READY_TO_DELIVER, DELIVERY_IN_PROGRESS, DELIVERED, CANCELED
}
