package fr.eservices.drive.controllers;

import fr.eservices.drive.dao.CartRepository;
import fr.eservices.drive.dao.OrderRepository;
import fr.eservices.drive.dao.StatusHistoryRepository;
import fr.eservices.drive.dao.StockRepository;
import fr.eservices.drive.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static fr.eservices.drive.controllers.Paths.AppPaths.APP_URL_PATTERN;
import static fr.eservices.drive.controllers.Paths.OrderPaths.ORDER_CONFIRM_ENDPOINT;

@Controller
public class OrderController extends AbstractController{

    @Autowired
    private OrderRepository orderRepo;
    @Autowired
    private StatusHistoryRepository statusHistoryRepo;
    @Autowired
    private StockRepository stockRepo;
    @Autowired
    private CartRepository cartRepo;

    @GetMapping(path = ORDER_CONFIRM_ENDPOINT)
    public String validateCart(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response){
        // redirect to login if not authenticated
        if (!isCustomerAuthenticatedIn(session)){
            return redirectToLogin();
        }
        Cart activeCart = getCartFrom(request);
        activeCart = activeCart != null ? activeCart : new Cart();


        Customer customer = getAuthenticatedCustomer(session);
        populateCart(customer, activeCart);
        List<CartEntry> cartEntries = activeCart.getCartEntries();

        Order order = saveOrderAndHisStatus(customer);

        clearActiveCart(customer);
        cartRepo.save(customer.getActiveCart());
        customerRepo.save(customer);

        reduceQuantityFor(cartEntries);
        populateHeaderData(model, activeCart);
        Cookie cookie = invalidateCartCookie();
        response.addCookie(cookie);
        model.addAttribute("amount", order.getAmount());
        return "order-confirmation";
    }

    private String redirectToLogin() {
        return "redirect:"+ APP_URL_PATTERN + "/login";
    }

    private Order saveOrderAndHisStatus(Customer customer) {
        Status orderStatus = Status.ORDERED;
        StatusHistory statusHistory = createStatusHistory(orderStatus);
        Order order = createOrder(customer.getActiveCart().getCartEntries());
        order.setCurrentStatus(orderStatus);
        order.getHistory().add(statusHistory);
        order.setCustomer(customer);
        statusHistoryRepo.save(statusHistory);
        orderRepo.save(order);
        return order;
    }

    private void reduceQuantityFor(List<CartEntry> cartEntries) {
        cartEntries.forEach(cartEntry -> {
            StockEntry stockEntry = stockRepo.findByArticle(cartEntry.getArticle());
            int consumedQuantity = cartEntry.getQuantity();
            stockEntry.reduceQuantity(consumedQuantity);
            stockRepo.save(stockEntry);
        });
    }

    private Order createOrder(List<CartEntry> cartEntries) {
        Order order = new Order();
        order.setAmount(amountCalculator.totalPrices(cartEntries));
        order.setArticles(articlesFrom(cartEntries));
        order.setCreatedOn(new Date());
        return order;
    }

    private List<Article> articlesFrom(List<CartEntry> cartEntries) {
        final List<Article> articles = new ArrayList<>();
        cartEntries.forEach(cartEntry ->
            articles.add(cartEntry.getArticle())
        );
        return articles;
    }

    private void clearActiveCart(Customer customer) {
        customer.getActiveCart().clear();
    }

    private StatusHistory createStatusHistory(Status orderStatus) {
        StatusHistory statusHistory = new StatusHistory();
        statusHistory.setStatus(orderStatus);
        statusHistory.setStatusDate(new Date());
        return statusHistory;
    }

    private void populateCart(Customer customer, Cart activeCart) {
        customer.setActiveCart(activeCart);
    }

}
