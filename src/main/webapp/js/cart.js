$(function () {
    $("a.update-cart").click(function (e) {
        e.preventDefault();
        var btn = $(this)
        var href = btn.attr('href');
        var input = btn.parent().siblings(".input-group").children(".form-control");
        $.ajax({
            type: "POST",
            headers: {'content-type': 'application/json', 'Accept': 'application/json'},
            dataType: 'json',
            data: JSON.stringify({'articleId': input.attr('id'), 'quantity': input.val()}),
            url: href,
            success: function (data) {
                if (data.status === "OK") {
                    $('#count').text(data.count);
                    $('#total').text(data.total + ' c');
                }
            }
        })
    });

    $("a.delete-entry").click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $.get(href, function (data) {
            if (data.status === "OK") {
                $('#count').text(data.count);
                $('#total').text(data.total + ' c');
            }
        });
    })
});