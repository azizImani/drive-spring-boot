## Steps to follow

You’ll start by unzipping this file, then follow the hereunder

1. Execute: mvn clean install
2. Execute: mvn spring-boot:run 
3. Go to http://localhost:9093/
4. Enjoy!